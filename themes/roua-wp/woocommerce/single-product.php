<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$prefix = Haze_Meta_Boxes::get_instance()->prefix;

get_header(); ?>

    <!-- ================================================== -->
    <!-- =============== START BREADCRUMB ================ -->
    <!-- ================================================== -->

    <?php if ( $cover_image = rwmb_meta("{$prefix}cover_image", array('type'=>'image_advanced', 'size'=>'full')) ): ?>

        <?php $cover_image = array_shift($cover_image); ?>

        <section class="no-mb">
            <div class="row">
                <div class="col-sm-12">
                    <div class="breadcrumb-fullscreen-parent">
                        <div class="breadcrumb breadcrumb-fullscreen alignleft" style="background-image: url(<?php echo $cover_image['url']; ?>);" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0"></div>
                    </div>
                </div>
            </div>
        </section>

    <?php elseif ( has_post_thumbnail() && ! post_password_required() ): ?>

        <?php
        $img_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
        $img_url = $img_url[0];
        ?>

        <section class="no-mb">
            <div class="row">
                <div class="col-sm-12">
                    <div class="breadcrumb-fullscreen-parent">
                        <div class="breadcrumb breadcrumb-fullscreen alignleft" style="background-image: url(<?php echo $img_url; ?>);" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0"></div>
                    </div>
                </div>
            </div>
        </section>

    <?php else: ?>

        <section>
            <div class="row">
                <div class="col-sm-12">
                    <div class="breadcrumb">
                        <h1>
                            <?php roua_upper_title(); ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

    <!-- ================================================== -->
    <!-- =============== END BREADCRUMB ================ -->
    <!-- ================================================== -->

    <!-- ================================================== -->
    <!-- =============== START CONTENT PAGE ================ -->
    <!-- ================================================== -->

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>