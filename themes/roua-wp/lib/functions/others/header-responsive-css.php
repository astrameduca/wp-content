<?php
$prefix = Haze_Meta_Boxes::get_instance()->prefix;

$tablet_image = rwmb_meta( "{$prefix}tablet_header_image", $args = array('type' => 'image_advanced', 'size' => 'full'), get_the_ID() );
$tablet_image = array_shift($tablet_image);

$phone_image = rwmb_meta( "{$prefix}phone_header_image", $args = array('type' => 'image_advanced', 'size' => 'full'), get_the_ID() );
$phone_image = array_shift($phone_image);
?>
<?php if($tablet_image || $phone_image): ?>
<style>
    <?php if($tablet_image): ?>
        @media (max-width: 1200px) {
            .breadcrumb.breadcrumb-fullscreen {
                background-image: url(<?php echo $tablet_image['full_url']; ?>) !important;
            }
        }
    <?php endif; ?>
    <?php if($phone_image): ?>
        @media (max-width: 500px) {
            .breadcrumb.breadcrumb-fullscreen {
                background-image: url(<?php echo $phone_image['full_url']; ?>) !important;
            }
        }
    <?php endif; ?>
</style>
<?php endif; ?>