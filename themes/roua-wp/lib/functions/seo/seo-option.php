<?php

function nm_callback_general_seo() {
    get_template_part('lib/functions/seo/seo-graphs');
}

/**
 * Facebook share correct image fix (thanks to yoast).
 */
function nm_callback_facebook_opengraph() {
    get_template_part('lib/functions/seo/facebook-graphs');
}

function load_social_share() {
    add_action( 'wp_head', 'nm_callback_general_seo' );
    add_action( 'wp_head', 'nm_callback_facebook_opengraph' );
}

add_action( 'init', 'load_social_share', 5 );