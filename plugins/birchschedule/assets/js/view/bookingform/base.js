(function($) {
    var params = birchschedule_view_bookingform;
    var scAttrs = birchschedule_view_bookingform_sc_attrs;
    var valueWeek = 1;
    var ns = birchpress.namespace('birchschedule.view.bookingform', {
        __init__: function() {
            var getNow4Locale = birchschedule.model.getNow4Locale;

            ns.setLocationOptions();
            ns.setServiceOptions();
            ns.setStaffOptions();
            ns.setStaffTable(1);
            ns.setAppointmentPrice();
            ns.setAppointmentDuration();
            ns.getMetaStaffMap();

           // var datepicker = ns.initDatepicker();

            $('#birs_appointment_location').on('change', function() {
                ns.setServiceOptions();
                ns.setStaffOptions();
                ns.setStaffTable(1);
                ns.setAppointmentPrice();
                ns.setAppointmentDuration();
                //ns.refreshDatepicker(datepicker);
                ns.reloadTimeOptions();
            });

            $('#birs_appointment_service').on('change', function() {
                ns.setStaffOptions();
                ns.setStaffTable(1);
                ns.setAppointmentPrice();
                ns.setAppointmentDuration();
                //ns.refreshDatepicker(datepicker);
                ns.reloadTimeOptions();
            });

            $('#birs_appointment_staff').on('change', function() {
                ns.setStaffTable(1);
               // ns.refreshDatepicker(datepicker);
                ns.reloadTimeOptions();
            });

            $('#birs_appointment_date').on('change', function() {
                ns.reloadTimeOptions();
            });

            $('#birs_book_appointment').click(function() {
                ns.bookAppointment();
            });
        },

        getServicesPricesMap: function() {
            return params.services_prices_map;
        },

        getMetaStaffMap: function() {
            return params.meta_staff_map;
        },
        getLocationsMap: function() {
            return params.locations_map;
        },

        getLocationsServicesMap: function() {
            return params.locations_services_map;
        },

        getLocationsStaffMap: function() {
            return params.locations_staff_map;
        },

        getServicesStaffMap: function() {
            return params.services_staff_map;
        },

        getLocationsOrder: function() {
            var locationIds = params.locations_order;
            if (scAttrs['location_ids']) {
                locationIds = _.intersection(scAttrs['location_ids'], locationIds);
            }
            return locationIds;
        },

        getServicesOrder: function() {
            var serviceIds = params.services_order;
            if (scAttrs['service_ids']) {
                serviceIds = _.intersection(scAttrs['service_ids'], serviceIds);
            }
            return serviceIds;
        },

        getStaffOrder: function() {
            var staffIds = params.staff_order;
            if (scAttrs['staff_ids']) {
                staffIds = _.intersection(scAttrs['staff_ids'], staffIds);
            }
            return staffIds;
        },

        getServicesDurationMap: function() {
            return params.services_duration_map;
        },

        setLocationOptions: function() {
            var locationsOrder = ns.getLocationsOrder();
            var locationsMap = ns.getLocationsMap();

            var options = birchschedule.model.getLocationOptions(locationsMap, locationsOrder);
            var html = birchschedule.view.getOptionsHtml(options);
            $('#birs_appointment_location').html(html);
        },

        setServiceOptions: function() {
            var locationsServicesMap = ns.getLocationsServicesMap();
            var servicesOrder = ns.getServicesOrder();
            var locationId = $('#birs_appointment_location').val();

            var options = birchschedule.model.getServiceOptions(locationsServicesMap,
                locationId, servicesOrder);
            var html = birchschedule.view.getOptionsHtml(options);

            var serviceId = parseInt($('#birs_appointment_service').val());
            $('#birs_appointment_service').html(html);

            if (serviceId && _(options.order).contains(serviceId)) {
                $('#birs_appointment_service').val(serviceId);
            }
        },

        setStaffTable: function(number){
            var locationsServicesMap = ns.getLocationsServicesMap();
            var servicesOrder = ns.getServicesOrder();
            var locationId = $('#birs_appointment_location').val();
            var serviceId = $('#birs_appointment_service').val();
            var locationsStaffMap = ns.getLocationsStaffMap();
            var servicesStaffMap = ns.getServicesStaffMap();
            var staffService = birchschedule.model.getServiceExistsOptions(locationsServicesMap,
                locationId, servicesOrder, servicesStaffMap);
            var staffOrder = ns.getStaffOrder();
            var metaStaffMap = ns.getMetaStaffMap();

            var options = birchschedule.model.getStaffData(locationsStaffMap, servicesStaffMap,
                locationId, serviceId, staffOrder, staffService);
            $('#staffTable').html(birchschedule.view.getStaffTableHtml(options, metaStaffMap));
            $('.schedule').on('click', function(event){
                $(this).attr('style', 'display:none');
                var id = event.toElement.id.replace('schedule_','');
                var gotoDate = birchschedule.model.getNow4Locale();
                if (scAttrs['date']) {
                    gotoDate = $.datepicker.parseDate('mm/dd/yy', scAttrs['date']);
                }
                $('#birs_appointment_datepicker').datepicker( 'destroy' );
                birchschedule.view.initDatepicker({
                    gotoDate: gotoDate
                }, id);
                var days = {};
                days[0]='ПН';
                days[1]='ВТ';
                days[2]='СР';
                days[3]='ЧТ';
                days[4]='ПТ';
                days[5]='СБ';
                days[6]='ВС';
                var months = {};
                months['Январь']='января';
                months['Февраль']='февраля';
                months['Март']='марта';
                months['Апрель']='апреля';
                months['Май']='мая';
                months['Июнь']='июня';
                months['Июль']='июля';
                months['Август']='августа';
                months['Сентябрь']='сентября';
                months['Октябрь']='октября';
                months['Ноябрь']='ноября';
                months['Декабрь']='декабря';
                var newTableBody ='<div class="row"><div class="btn-week btn-primary"><span><</span></div>';
                var indexOfToday=-1;
                var indexOrder=1;
                $('.ui-datepicker-group' ).each(function(  ) {
                    var month = $(this).find(".ui-datepicker-month").text();
                    $(this).find('td' ).each(function(index) {
                        if($(this).attr('class').indexOf('ui-datepicker-today')>-1){
                            indexOfToday=index;
                        }
                        if(indexOfToday > -1 && indexOrder <= number*7&& $(this).text().trim() != '') {
                            indexOrder++;
                            var mod = index % 7;
                            var clas = $(this).attr('class') + ' col-xs-1 element-datapicker';
                            var newTd ='';
                            if(clas.indexOf('ui-state-disabled') >-1){
                            newTd = '<div id="day_' + index + '" class="' + clas + '"><span>' + days[mod] + '</span>' +
                                '<div class="schedule-day-month">' + $(this).html() +'<span class="schedule-month"> ' + months[month]
                            + '</span></div><div class="btn-primary schedule-no-time">Приема нет</div> </div>';
                            } else{
                                var month_ = $(this).attr("data-month");
                                month_ < 12 ? month_++ : month_;
                                var data = month_ + '%2F' + $(this).find("a").text() + '%2F' + $(this).attr("data-year");;
                                newTd = '<div id="day_' + index + '" class="' + clas + '"><span>' + days[mod] + '</span>' +
                                '<div class="schedule-day-month">' + $(this).html() +'<span class="schedule-month"> ' + months[month] + '</span></div>' +
                                '<div data-handler="selectDay" data="'+ data +'" class="btn-primary schedule-time">' +
                                //'Записаться' +
                                '<ul class="nav nav-pills">' +
                                '<li class="dropdown">' +
                                '<a class="dropdown-toggle" data-toggle="dropdown" href="#">Записаться</a>' +
                                '<ul id="menu1" class="dropdown-menu">' +
                                '<li></li>' +
                                '</ul>' +
                                '</li>' +
                                '</ul>' +


                                '<div></div></div> </div>';
                            }
                            $(this).html(newTd);
                            $(this).attr('id', 'day_' + index);
                            newTableBody = newTableBody + $(this).html();
                        }
                    });


                });
                newTableBody =newTableBody + '<div class="btn-week btn-primary"><span>></span></div></div>';
                $('#datapicker_'+id).html(newTableBody);

                $("[data-handler='selectDay']").on('click', function(e){
                    e.preventDefault();
                    var i18n = birchschedule.view.getI18nMessages();
                    var pluginUrl = birchschedule.view.getPluginUrl();
                    var waitImgUrl = pluginUrl + '/assets/images/ajax-loader.gif';
                    var waiting_html = "<div id='birs_appointment_timeoptions'>" +
                        i18n['Please wait...'] +
                        "<img src=" + waitImgUrl + " />" +
                        "</div>";
                    $('.birs_form_field.birs_appointment_time .birs_field_content').html(waiting_html);
                    $('#birs_appointment_alternative_staff').val('');
                    $('#birs_appointment_time').val('');
                    var ajaxUrl = birchschedule.model.getAjaxUrl();
                    $('#birs_appointment_avaliable_staff').attr('value',id);
                    $('#birs_appointment_staff').attr('value',id);
                    var postData = ns.getFormQueryData();
                    postData += '&' + $.param({
                        action: 'birchschedule_view_bookingform_get_avaliable_time'
                    });
                    postData = postData.replace('birs_appointment_date=',
                        'birs_appointment_date='+ $(this).attr("data"));
                    var tempId=(Math.random() + '').split ('.') [1];
                    localStorage.setItem("tempId", tempId);
                    $(this).attr("id", tempId);
                    $.post(ajaxUrl, postData, function(data, status, xhr) {
                        $('.birs_form_field.birs_appointment_time .birs_field_content').html(data);
                        var id = '#'+localStorage.getItem('tempId') + ' .dropdown-menu';
                        $(id).html(data);
                        ns.onTimeOptionsLoad();
                    }, 'html');

                });
                return true;
            });
            $('.btn-week').on('click', function(){
                var direction = $(this).attr("direction");
                if(direction == 'next'){
                    if(valueWeek <3)
                        valueWeek++;
                } else {
                    if(valueWeek >1)
                        valueWeek --;
                }
            });
        },

        setStaffOptions: function() {
            var locationId = $('#birs_appointment_location').val();
            var serviceId = $('#birs_appointment_service').val();
            var locationsStaffMap = ns.getLocationsStaffMap();
            var servicesStaffMap = ns.getServicesStaffMap();
            var staffOrder = ns.getStaffOrder();

            var options = birchschedule.model.getStaffOptions(locationsStaffMap, servicesStaffMap,
                locationId, serviceId, staffOrder);
            var html = birchschedule.view.getOptionsHtml(options);

            var staffId = parseInt($('#birs_appointment_staff').val());
            $('#birs_appointment_staff').html(html);
            var avaliableStaff = options.order.join();
            $('#birs_appointment_avaliable_staff').val(avaliableStaff);

            if (staffId && _(options.order).contains(staffId)) {
                $('#birs_appointment_staff').val(staffId);
            }
        },

        getFormQueryData: function() {
            var postData = $('#birs_appointment_form').serialize();
            return postData;
        },

        selectTime: function(el) {
            $('#birs_appointment_time').val($(el).attr('data-time'));

            var alternativeStaff = $(el).attr('data-alternative-staff');
            $('#birs_appointment_alternative_staff').val(alternativeStaff);

            $('#birs_appointment_timeoptions .birs_option').removeClass('selected');
            $(el).addClass('selected');
        },

        reloadTimeOptions: function() {
            var dateValue = $('#birs_appointment_date').val();
            if (!dateValue) {
                return;
            }

            var i18n = birchschedule.view.getI18nMessages();
            var pluginUrl = birchschedule.view.getPluginUrl();
            var waitImgUrl = pluginUrl + '/assets/images/ajax-loader.gif';
            var waiting_html = "<div id='birs_appointment_timeoptions'>" +
                i18n['Please wait...'] +
                "<img src=" + waitImgUrl + " />" +
                "</div>";
            $('.birs_form_field.birs_appointment_time .birs_field_content').html(waiting_html);
            $('#birs_appointment_alternative_staff').val('');
            $('#birs_appointment_time').val('');

            var ajaxUrl = birchschedule.model.getAjaxUrl();
            var postData = ns.getFormQueryData();
            postData += '&' + $.param({
                action: 'birchschedule_view_bookingform_get_avaliable_time'
            });
            $.post(ajaxUrl, postData, function(data, status, xhr) {
                $('.birs_form_field.birs_appointment_time .birs_field_content').html(data);
                ns.onTimeOptionsLoad();
            }, 'html');
        },

        onTimeOptionsLoad: function() {
            $('#birs_appointment_timeoptions .birs_option').click(function(e) {
                ns.selectTime(e.target);
            });
            $('#birs_appointment_timeoptions').change(function() {
                ns.selectTime($('#birs_appointment_timeoptions option:selected'));
            });
        },

        setAppointmentPrice: function() {
            var serviceId = $('#birs_appointment_service').val();
            if (serviceId) {
                $('#birs_appointment_price').val(ns.getServicesPricesMap()[serviceId].price);
            }
        },

        setAppointmentDuration: function() {
            var serviceId = $('#birs_appointment_service').val();
            if (serviceId) {
                $('#birs_appointment_duration').val(ns.getServicesDurationMap()[serviceId].duration);
            }
        },

        initDatepicker: function() {
            var gotoDate = birchschedule.model.getNow4Locale();
            if (scAttrs['date']) {
                gotoDate = $.datepicker.parseDate('mm/dd/yy', scAttrs['date']);
            }
            return birchschedule.view.initDatepicker({
                gotoDate: gotoDate
            });
        },

        refreshDatepicker: function(datepicker) {
            birchschedule.view.refreshDatepicker(datepicker);
        },

        bookSucceed: function(message) {
            var fns = {};
            fns['text'] = function(message) {
                $('.birs_error').hide("");
                $('#birs_booking_box').hide();
                $('#birs_booking_success').html(message);
                $('#birs_booking_success').show("slow", function() {
                    birchpress.util.scrollTo(
                        $("#birs_booking_success"),
                        600, -40);
                });
            }
            return fns;
        },

        bookingSucceed: function(fn, message) {
            fn(message);
        },

        bookAppointment: function() {
            var ajaxUrl = birchschedule.model.getAjaxUrl();
            var postData = $('form').serialize();
            postData += '&' + $.param({
                action: 'birchschedule_view_bookingform_schedule'
            });
            $.post(ajaxUrl, postData, function(data, status, xhr) {
                $('#birs_please_wait').hide("slow");
                var result = birchschedule.model.parseAjaxResponse(data);
                if (result.errors) {
                    if( (typeof grecaptcha != 'undefined') && $('.gglcptch_recaptcha').length) {
                        grecaptcha.reset();
                    }
                    birchschedule.view.showFormErrors(result.errors);
                } else if (result.success) {
                    var bookSucceed = ns.bookSucceed();
                    ns.bookingSucceed(bookSucceed[result.success.code], result.success.message);
                }
            });
            $('.birs_error').hide("");
            $('#birs_please_wait').show("slow");
        },

        defineRule: function() {
            var conditionalLogic = function() {
                if (that._compare()) {
                    that._trueAction();
                } else {
                    that._falseAction();
                }
            };

            var that = {
                _whenField: '',

                _conditionValue: '',

                _compare: function() {
                    return true;
                },

                _trueAction: function() {},

                _falseAction: function() {},

                _apply: function() {
                    birchpress.addAction('birchschedule.view.bookingform.setLocationOptionsAfter', conditionalLogic);
                    $('#' + that._whenField).change(conditionalLogic);
                },

                when: function(whenField) {
                    that._whenField = 'birs_' + whenField;
                    return that;
                },

                is: function(conditionValue) {
                    that._conditionValue = conditionValue;
                    that._compare = function() {
                        var fieldValue = $('#' + that._whenField).val();
                        return that._conditionValue == fieldValue;
                    };
                    return that;
                },

                hide: function(targetField) {
                    var selector = '.birs_' + targetField;
                    that._trueAction = function() {
                        $(selector).hide();
                    };
                    that._falseAction = function() {
                        $(selector).show();
                    };
                    that._apply();
                },

                show: function(targetField) {
                    var selector = '.birs_' + targetField;
                    that._trueAction = function() {
                        $(selector).show();
                    };
                    that._falseAction = function() {
                        $(selector).hide();
                    };
                    that._apply();
                }
            };
            return that;
        }
    });
})(jQuery);