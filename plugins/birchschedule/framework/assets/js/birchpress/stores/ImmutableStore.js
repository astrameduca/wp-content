'use strict';

var Immutable = require('immutable');
var Cursor = require('immutable/contrib/cursor');
var lang = require('../lang');

var clazz = lang.provide('birchpress.stores.ImmutableStore', {

  __construct__: function(self, data) {
    self._cursor = self._createCursor(data);
  },

  getCursor: function(self) {
    return self._cursor;
  },

  _createCursor: function(self, data) {
    return Cursor.from(data, function(newData) {
      self._cursor = self._createCursor(newData);
      self.onChange(self.getCursor());
    });
  },

  onChange: function(self, newCursor) {}

});

module.exports = clazz;
