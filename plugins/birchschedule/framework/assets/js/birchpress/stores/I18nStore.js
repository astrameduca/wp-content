'use strict';

var Immutable = require('immutable');
var _ = require('underscore');
var lang = require('../lang');
var ImmutableStore = require('./ImmutableStore');

var clazz = lang.provide('birchpress.stores.I18nStore', {

  __construct__: function __construct__(self) {
    var data = Immutable.fromJS({});
    var immutableStore = ImmutableStore(data);
    immutableStore.addAction('onChangeAfter', function(newCursor) {
      self.onChange();
    });
    self._immutableStore = immutableStore;
  },

  getCursor: function getCursor(self) {
    return self._immutableStore.getCursor();
  },

  onChange: function onChange(self) {},

  loadTranslations: function loadTranslations(self, translations) {
    self.getCursor().set('translations', Immutable.fromJS(translations.entries));
  },

  getText: function getText(self, string) {
    var translations = self.getCursor().get('translations').deref();
    var result;

    if (Immutable.Map.isMap(translations)) {
      result = translations.getIn([string, 'translations', 0], string);
      if (_.isEmpty(result)) {
        result = string;
      }
    } else {
      result = string;
    }
    return result;
  },

  __: function __(self, string) {
    return self.getText(string);
  }

});

module.exports = clazz;

