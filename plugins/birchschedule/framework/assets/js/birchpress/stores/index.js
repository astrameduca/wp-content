'use strict';

var lang = require('../lang');
var ns;

ns = lang.provide('birchpress.stores', {});

require('./ImmutableStore');
require('./I18nStore');

module.exports = ns;
