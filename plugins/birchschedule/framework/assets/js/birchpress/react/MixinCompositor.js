'use strict';

var _ = require('underscore');
var lang = require('../lang');

var ns = lang.provide('birchpress.react.MixinCompositor', {

  getReactMixins: function(component) {
    return [];
  },

  componentWillMount: function(component) {
    var mixins = component.getReactMixins();
    _.each(mixins, function(mixin) {
      if (_.isFunction(mixin.componentWillMount)) {
        mixin.componentWillMount.call(component);
      }
    });
  },

  componentDidMount: function(component) {
    var mixins = component.getReactMixins();
    _.each(mixins, function(mixin) {
      if (_.isFunction(mixin.componentDidMount)) {
        mixin.componentDidMount.call(component);
      }
    });
  },

  componentWillReceiveProps: function(component, nextProps) {
    var mixins = component.getReactMixins();
    _.each(mixins, function(mixin) {
      if (_.isFunction(mixin.componentWillReceiveProps)) {
        mixin.componentWillReceiveProps.call(component, nextProps);
      }
    });
  },

  shouldComponentUpdate: function(component, nextProps, nextState) {
    var result = true;
    var mixins = component.getReactMixins();
    var mergedMixin = {};
    _.each(mixins, function(mixin) {
      _.extend(mergedMixin, mixin);
    });
    if (_.isFunction(mergedMixin.shouldComponentUpdate)) {
      result = mergedMixin.shouldComponentUpdate.call(component, nextProps, nextState);
    }
    return result;
  },

  componentWillUpdate: function(component, nextProps, nextState) {
    var mixins = component.getReactMixins();
    _.each(mixins, function(mixin) {
      if (_.isFunction(mixin.componentWillUpdate)) {
        mixin.componentWillUpdate.call(component, nextProps, nextState);
      }
    });
  },

  componentDidUpdate: function(component, prevProps, prevState) {
    var mixins = component.getReactMixins();
    _.each(mixins, function(mixin) {
      if (_.isFunction(mixin.componentDidUpdate)) {
        mixin.componentDidUpdate.call(component, prevProps, prevState);
      }
    });
  },

  componentWillUnmount: function(component) {
    var mixins = component.getReactMixins();
    _.each(mixins, function(mixin) {
      if (_.isFunction(mixin.componentWillUnmount)) {
        mixin.componentWillUnmount.call(component);
      }
    });
  }
});

module.exports = ns;
