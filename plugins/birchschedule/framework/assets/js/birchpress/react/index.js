'use strict';

var _ = require('underscore');
var React = require('react');
var lang = require('../lang');
var reactCreateElement = React.createElement;
var ns;

ns = lang.provide('birchpress.react', {

  toReactClass: function toReactClass(clazz) {
    var spec = {};
    var keys = _.keys(clazz);
    var specialMethods = lang.getSpecialWords();
    _.each(keys, function(key) {
      if (_.contains(specialMethods, key)) {
        if (key === '__statics__') {
          spec.statics = {};
          _.each(clazz.__statics__, function(name) {
            spec.statics[name] = clazz[name];
          });
        }
        return;
      }
      if (_.contains(clazz.__statics__, key)) {
        return;
      }
      if (_.isFunction(clazz[key])) {
        spec[key] = function() {
          return _.partial(clazz[key], this).apply(this, arguments);
        };
      } else {
        spec[key] = clazz[key];
      }
    });
    return React.createClass(spec);
  },

  createElement: function createElement() {
    var type;
    var args = Array.prototype.slice.call(arguments);
    var clazz = args.shift();

    if (_.has(clazz, '__fullname__')) {
      type = birchpress.react.toReactClass(clazz);
    } else {
      type = clazz;
    }

    return _.partial(reactCreateElement, type).apply(null, args);
  }
});

React.createElement = ns.createElement;

require('./MixinCompositor');

module.exports = ns;
